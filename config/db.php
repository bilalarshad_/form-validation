<?php
/* This piece of code establishes connection with database */
$host = "localhost";
$database_user = "root";
$database_password = "";
$database_name="form_validation";

$connection = new mysqli($host,$database_user,$database_password,$database_name);
if($connection->connect_error){
    die("Database connection Could Not Be Established\n".$connection->connect_error);
}
// else{
//     echo "Database Connection Successfully Established";
// }
?>