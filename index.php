<?php
    include "templates/header.php";
    $name_error = $email_error = $url_error = $gender_error = "";
    $name = $email = $url = $gender = "";
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if (empty($_POST['name'])) {
            $name_error = "This Field is Required";
        } else {
            $name = $_POST['name'];
            $name = trim(stripslashes($name));
            $email = $_POST['email'];
            //Validation of Name Field
            $name_pattern = '/^[\w*\'?\s*\'?]*$/';
            if (!preg_match($name_pattern, $name)) {
                $name_error = "Only uses Letters and spaces";
                $name = "";
            }
        }
        if (empty($_POST['email'])) {
            $email_error = "This Field is Required";
        } else {
            $email = $_POST['email'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email_error = "Enter Email in Valid Format";
                $email = "";
            }
        }
        if (empty($_POST['gender'])) {
            $gender_error = "This Field is Required";
        } else {
            $gender = $_POST['gender'];
        }

        $url = $_POST['url'];
        if (!empty($url)) {
            if (!filter_var($url, FILTER_VALIDATE_URL)) {
                $url_error = "Enter a Valid URL";
                $url = "";
            }
        }
    }

    ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="mt-5">
                    <h1 class="page-head mt-5 text-center">PHP Form Validation and Submission into Database</h1>
                    <form class="mt-5" method="post" action="<?= htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                        <div class="form-group">
                            <label for="name">Name<span class="required-symbol">*</span></label>
                            <div class="form-flex">
                                <input type="text" class="form-control" name="name" placeholder="John Doe">
                            </div>
                            <div class="error"><?= $name_error; ?></div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email <span class="required-symbol">*</span></label>
                            <div class="form-flex">
                                <input type="text" class="form-control" name="email" placeholder="john@example.com">
                            </div>
                            <div class="error"><?= $email_error; ?></div>
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender <span class="required-symbol">*</span></label>
                            <div class="form-flex gender-input">
                                <label><input type="radio" name="gender" value="male">&nbsp;Male</label>
                                <label><input type="radio" name="gender" value="female">&nbsp;Female</label>
                                <label><input type="radio" name="gender" value="other">&nbsp;Other</label>
                            </div>
                            <div class="error"><?= $gender_error; ?></div>
                        </div>
                        <div class="form-group">
                            <label for="url">Website</label>
                            <div class="form-flex">
                                <input type="text" class="form-control" name="url" placeholder="https://example.com/">
                            </div>
                            <div class="error"><?= $url_error; ?></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if(!empty($name)){?>
                <div class="form-data">
                    <?php $name = htmlspecialchars($name);
                    echo "Name: ".$name;
                    ?>
                </div>
                <?php } if(!empty($email)){?>
                <div class="form-data">
                    <?php $email = filter_var($email,FILTER_SANITIZE_EMAIL);
                    echo "Email: ".$email;
                    ?>
                </div>
                <?php }if(!empty($gender)){?>
                <div class="form-data">
                    <?= "Gender: ".$gender;
                    ?>
                </div>
                <?php }if(!empty($url)){?>
                <div class="form-data">
                    <?php $website = filter_var($url,FILTER_SANITIZE_URL);
                    echo "Website: ".$url;
                    ?>
                </div>
                <?php }?>
                
            </div>
        </div>
    </div>
<?php include "templates/footer.php";?>