<footer class="mt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="d-flex flex-row justify-content-center">
                    <img width="150" src="assets/media/php-logo.png">
                </div>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</footer>
</body>

</html>